import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { RutasAutenticadas } from './Componentes/Autenticados/RutasAutenticadas';
import { RutasNoAutenticadas } from './Componentes/NoAutenticados/RutasNoAutenticadas';

import { autenticacion } from './Store/Servicios/Firebase';
import { actionEstablecerSesion, actionCerrarSesion } from './Store/ACCIONES';

import { UPLOAD_SPRING_IMAGE } from 'react-native-dotenv';


class Seleccion extends Component {

  componentDidMount() {
    this.props.autenticacion();
    console.log(UPLOAD_SPRING_IMAGE);
  }

  render() {
    return (
      <View style={styles.container}>
        {this.props.usuario ? <RutasAutenticadas /> : <RutasNoAutenticadas />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = state => ({
  usuario: state.reducerSesion,
});

const mapDispatchToProps = dispatch => ({
  autenticacion: () => { 
    // podria pasarle valores de localstorage
    autenticacion.onAuthStateChanged((usuario) => {
      if (usuario) {
        console.log(usuario.toJSON());
        dispatch(actionEstablecerSesion(usuario));
      } else {
        console.log('No existe sesión');
        dispatch(actionCerrarSesion());
      }
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Seleccion);
