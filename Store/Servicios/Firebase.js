import { apiKey, authDomain, databaseURL, projectId, storageBucket, messagingSenderId } from 'react-native-dotenv';

import * as firebase from 'firebase';

const config = {
  apiKey,
  authDomain,
  databaseURL,
  projectId,
  storageBucket,
  messagingSenderId,
};
firebase.initializeApp(config);

export const autenticacion = firebase.auth();
export const baseDeDatos = firebase.database();

export const registroEnFirebase = values =>
  autenticacion
    .createUserWithEmailAndPassword(values.correo, values.password)
    .then(success => success.toJSON());

export const registroEnBaseDeDatos = ({uid, email, nombre, fotoURL}) =>
  baseDeDatos.ref(`usuarios/${uid}`).set({
    nombre,
    email,
    fotoURL,
  });

export const loginEnFirebase = ({ correo, password }) =>
  autenticacion.signInWithEmailAndPassword(correo, password).then(success => success.toJSON());

export const descargarAutor = uid =>
  baseDeDatos
    .ref(`usuarios/${uid}`)
    .once('value')
    .then(snapshot => snapshot.val());