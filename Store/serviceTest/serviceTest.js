//import Immutable from 'immutable'

import axios from 'axios';
import { baseDeDatos } from '../Servicios/Firebase';

/* Types */
export const ACTION_EXAMPLE = 'ACTION_EXAMPLE';
export const GET_REPOS_FAIL = 'GET_REPOS_FAIL';

const initialState = { repos:[] };

export default (state = initialState, action) => {
	switch (action.type) {
		case ACTION_EXAMPLE:
			//console.log("entra agirna state")
            return { ...state, repos: action.Obj };
        default:
			return state;
	}
}

/* Action Creators */
export const getCorridasAbiertas = () => {
	return (dispatch) => {
			return axios.get('https://facebook.github.io/react-native/movies.json')
			.then( response => {
				//console.log("entra 1 ")
				dispatch({ type: ACTION_EXAMPLE, Obj: response.data.movies });
				//console.log("entra 22")
				return response.data.movies;
			})
			.catch(error => {
				dispatch({ type: GET_REPOS_FAIL, Obj: {}});
				throw error
			});
	}
}


/**
 * Ejemplo con firebase usando redux thunk "bueno"
 */
export const getDatos = () => {

	return (dispatch) => {
 
		return baseDeDatos
		.ref('publicaciones/')
		.once('value')
		.then((snapshot) => {
		  const publicaciones = [];
		  snapshot.forEach((childSnapshot) => {
			const { key } = childSnapshot;
			const publicacion = childSnapshot.val();
			publicacion.key = key;
			publicaciones.push(publicacion);
		  });
		  return publicaciones;
		});
	}
}

export const getSetDonataria = (form) => {
	return (dispatch) => {
		dispatch({ type: ACTION_GET_SET_FILTER, Obj: form })
		let promise = new Promise(function(resolve) {
			resolve(form);
		})
		return promise
	}
}


