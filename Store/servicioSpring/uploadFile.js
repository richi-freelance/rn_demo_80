import axios from 'axios';
import { Platform } from 'react-native';
import { UPLOAD_SPRING_IMAGE } from 'react-native-dotenv';

export const uploadImage = ({ imagen }) => {
  
    const { uri } = imagen;
    const splitName = uri.split('/');
    const name = new Date().getTime() + [...splitName].pop();
  
    let source;
    if (Platform.OS === 'android') {
      source = {uri: uri, isStatic: true};
    } else {
      source = {uri: uri.replace('file://', ''), isStatic: true};
    }
  
    const foto = {
      uri: source.uri,
      type: 'image/jpeg',
      name
    };
    const url = Platform.OS === 'android' ? UPLOAD_SPRING_IMAGE : 'http://localhost:8080/uploadFile'; // genymotion's localhost is 10.0.3.2
  
    const formData = new FormData();
    formData.append('file', foto);
    const config = {
        headers: {
            "Content-Type": "multipart/form-data"        
        }
    };
    return axios.post(url, formData, config)
      .then((response) => {
        let data = Object.assign({}, response.data);
        data.width=data.height="200";
        data.secure_url = data.fileDownloadUri;
        return data;
      })
      .catch((error) =>{ return error; });
};