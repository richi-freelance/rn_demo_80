import { takeEvery, call, select, put, all } from 'redux-saga/effects';
import { baseDeDatos, loginEnFirebase, descargarAutor } from '../Servicios/Firebase';
import CONSTANTES from '../CONSTANTES';
import {
  actionAgregarPublicacionesStore,
  actionAgregarAutoresStore,
  actionExitoSubirPublicacion,
  actionErrorSubirPublicacion,
} from '../ACCIONES';

import { uploadImage } from '../servicioSpring/uploadFile';
import { sagaRegistro } from '../login/login';

function* sagaLogin(values) {
  try {
    const resultado = yield call(loginEnFirebase, values.datos);
  } catch (error) {
    console.log(error);
  }
}

const escribirFirebase = ({
  width, height, secure_url, uid,
}, texto = '') =>
  baseDeDatos
    .ref('publicaciones/')
    .push({
      width,
      height,
      secure_url,
      uid,
      texto,
    })
    .then(response => response);

const escribirAutorPublicaciones = ({ uid, key }) =>
  baseDeDatos
    .ref(`autor-publicaciones/${uid}`)
    .update({ [key]: true })
    .then(response => response);

function* sagaSubirPublicacion({ values }) {
  try {
    const imagen = yield select(state => state.reducerImagenPublicacion);
    const usuario = yield select(state => state.reducerSesion);
    const { uid } = usuario;

    const resultadoImagen = yield call(uploadImage, imagen);

    const { width, height, secure_url } = resultadoImagen;
    const parametrosImagen = {
      width,
      height,
      secure_url,
      uid,
    };
    const escribirEnFirebase = yield call(escribirFirebase, parametrosImagen, values.texto);

    const { key } = escribirEnFirebase;
    const parametrosAutorPublicaciones = { uid, key };
    const resultadoEscribirAutorPublicaciones = yield call(
      escribirAutorPublicaciones,
      parametrosAutorPublicaciones,
    );

    yield put(actionExitoSubirPublicacion());
  } catch (error) {
    console.log(error);
    yield put(actionErrorSubirPublicacion());
  }
}

const descargarPublicaciones = () =>
  baseDeDatos
    .ref('publicaciones/')
    .once('value')
    .then((snapshot) => {
      const publicaciones = [];
      snapshot.forEach((childSnapshot) => {
        const { key } = childSnapshot;
        const publicacion = childSnapshot.val();
        publicacion.key = key;
        publicaciones.push(publicacion);
      });
      return publicaciones;
    });

function* sagaDescargarPublicaciones() {
  try {
    const publicaciones = yield call(descargarPublicaciones);
    const autores = yield all(publicaciones.map(publicacion => call(descargarAutor, publicacion.uid)));

    yield put(actionAgregarAutoresStore(autores));
    yield put(actionAgregarPublicacionesStore(publicaciones));

    //return [autores, publicaciones];
  } catch (error) {
    console.log(error);
  }
}

export default function* funcionPrimaria() {
  yield takeEvery(CONSTANTES.REGISTRO, sagaRegistro);
  yield takeEvery(CONSTANTES.LOGIN, sagaLogin);
  yield takeEvery(CONSTANTES.SUBIR_PUBLICACION, sagaSubirPublicacion);
  yield takeEvery(CONSTANTES.DESCARGAR_PUBLICACIONES, sagaDescargarPublicaciones);
  // yield ES6
  console.log('Desde nuestra función generadora');
}
