import { call, select } from 'redux-saga/effects';
import { registroEnFirebase, registroEnBaseDeDatos } from '../Servicios/Firebase';
import { uploadImage } from '../servicioSpring/uploadFile';

const REGISTRO = 'REGISTRO';

export const actionRegistro = values => ({
    type: REGISTRO,
    datos: values,
});

export function* sagaRegistro(values) {
    try {
      const imagen = yield select(state => state.reducerImagenSignUp);
      const urlFoto = yield call(uploadImage, imagen);
  
      const fotoURL = urlFoto.secure_url;
      const registro = yield call(registroEnFirebase, values.datos);
      const { email, uid } = registro;
      const { datos: { nombre } } = values;

      yield call(registroEnBaseDeDatos, {
        uid,
        email,
        nombre,
        fotoURL,
      });
    } catch (error) {
      console.log(error);
    }
}
  