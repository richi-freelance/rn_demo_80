import { TabNavigator } from 'react-navigation';
import { StackHome } from './StackHome';
import { StackFollow } from './StackFollow';
import Profile from './Profile';
import { StackAdd } from './StackAdd';

const RutasAutenticadas = TabNavigator(
  {
    Home: {
      screen: StackHome,
    },
    Add: {
      screen: StackAdd,
    },
    Follow: {
      screen: StackFollow,
    },
    Profile: {
      screen: Profile,
    },
  },
  {
    tabBarPosition: 'bottom',
  },
);

export { RutasAutenticadas };
