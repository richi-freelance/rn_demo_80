import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Button, FlatList, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { actionDescargarPublicaciones } from '../../Store/ACCIONES';
import { getCorridasAbiertas, getDatos } from '../../Store/serviceTest/serviceTest';
import Publicacion from './Publicacion';

class Home extends Component {
  
  componentDidMount() {
    this.props.descargarPublicaciones();
  }

  componentWillMount() {
    this.props.getCorridasAbiertas().then( res =>{console.log("res", res)} );
    
    // ejemplo con firebase
    //this.props.getDatos().then( res =>{console.log("res", res)} );
  }


  componentWillReceiveProps(nextProps) {
    console.log("nextProps.repos", nextProps.repos);
  }
 


  render() {

    const { navigation, autores } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.publicaciones}
          renderItem={({ item, index }) => <Publicacion item={item} autor={autores[index]} />}
          ItemSeparatorComponent={() => <View style={styles.separador} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  separador: {
    borderWidth: 1,
    borderColor: '#f70000',
  },
});


const mapStateToProps = state => {

  return {
    publicaciones: state.reducerPublicacionesDescargadas,
    autores: state.reducerAutoresDescargados,
    repos: state.reducerServiceTest,
  };
};

const mapDispatchToProps = (dispatch) => {
	return {
    getDatos: ()=> dispatch(getDatos()),
    descargarPublicaciones: () => dispatch(actionDescargarPublicaciones()),
    getCorridasAbiertas: () => dispatch(getCorridasAbiertas()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);